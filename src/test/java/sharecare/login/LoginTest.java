package sharecare.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
public class LoginTest {
 WebDriver driver;

 @BeforeTest
 public void StartBrowser() {
 System.setProperty("webdriver.chrome.driver","drivers/chromedriver.exe");
 driver = new ChromeDriver();
 driver.manage().window().maximize();
 }

 @AfterTest
 public void CloseBrowser() throws Exception{
	 Thread.sleep(10000);
	 
  driver.quit();
 }

 //@Test(dataProvider="UserCredentials")
 @Parameters({"uname","password"})
 @Test
 public void testToLogin(String uname,String password) throws Exception{
	 Login login= new Login(driver);
	 login.loginToSharecare(uname, password);
	 login.verifyPageTitle();
 }
 
// @DataProvider(name="UserCredentials")
// public Object[][] getDataFromDataprovider(){
// return new Object[][] 
// 	{
//         { "Yalapatireddypavan@gmail.com", "Ypkr66@gmail" }
//     };
// }
 
}
