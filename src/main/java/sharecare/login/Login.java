package sharecare.login;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Login {

	WebDriver driver;

	public Login(WebDriver driver) {
		super();
		this.driver = driver;
	}

	// Page Object Locators For Login Page
	private static final String URL = "https://you.sharecare.com/";

	private static final By EMAIL = By.xpath("//input[@id='input.username']");
	private static final By PASSWORD = By.xpath("//input[@id='input.password']");
	private static final By SIGN_IN = By.xpath("//button[.//text()='Sign In']");
	private static final String EXPECTED_PAGE_TITLE = "Sharecare";
	private static final String READY_STATE_COMPLETE = "complete";

	/**
	 * This method takes Username and Password as parameters and then logs into the
	 * sharecare application. It throws NullPoint Exception
	 * 
	 * @param uname
	 * @param password
	 * @throws Exception
	 */
	public void loginToSharecare(String uname, String password) throws Exception {

		driver.get(URL);
		WebDriverWait wait = new WebDriverWait(driver, 10000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(EMAIL));
		driver.findElement(EMAIL).sendKeys(uname);
		driver.findElement(PASSWORD).sendKeys(password);
		driver.findElement(SIGN_IN).click();

	}

	/**
	 * This method verifies the title of the page upon login to the Sharecare
	 * application
	 * 
	 * @throws Exception
	 */
	public void verifyPageTitle() throws Exception {
		// Wait until Html DOM loads
		waitForPageLoad();

		// Get the current page title
		String actualTitle = driver.getTitle();

		// Verify the title using TestNG Assertion
		Assert.assertEquals(actualTitle, EXPECTED_PAGE_TITLE);
	}

	/**
	 * This method makes the wed driver wait until Html DOM fully loaded
	 */
	private void waitForPageLoad() {
		String readyState = null;
		Boolean isReadyStateComplete = false;
		while (!isReadyStateComplete) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			readyState = (String) jse.executeScript("return document.readyState").toString();

			if (readyState.contentEquals(READY_STATE_COMPLETE)) {
				isReadyStateComplete = true;
			}
		}
	}

}
